package service

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/fighters-solution/ttcourses/config"
	"gitlab.com/fighters-solution/ttcourses/model"
	"gitlab.com/fighters-solution/ttcourses/repository"

	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
)

var repo *repository.CourseRepo

type CourseService struct {
	repo *repository.CourseRepo
}

func NewCourseService(cfg *config.Config) *CourseService {
	var err error
	repo, err = repository.NewCourseRepo(cfg)
	if err != nil {
		panic(err)
	}
	return &CourseService{
		repo: repo,
	}
}

func (srv *CourseService) FindAllCourses(w http.ResponseWriter, r *http.Request) {
	courses, err := repo.FindAllCourses()
	HandleError(err)
	if err := json.NewEncoder(w).Encode(courses); err != nil {
		render.JSON(w, r, err)
	}
}

func (srv *CourseService) FindCoursesByName(w http.ResponseWriter, r *http.Request) {
	parm := chi.URLParam(r, "name")
	courses, err := repo.GetCourseByName(parm)
	if err != nil && strings.Contains(err.Error(), "not found") {
		render.JSON(w, r, err)
	}
	if err := json.NewEncoder(w).Encode(courses); err != nil {
		render.JSON(w, r, err)
	}
}

func (srv *CourseService) FindCourseByNameIgnoreCase(w http.ResponseWriter, r *http.Request) {
	parm := chi.URLParam(r, "alias")
	course, err := repo.GetCourseByNameIgnoreCase(strings.ToLower(parm))
	HandleError(err)
	if err := json.NewEncoder(w).Encode(course); err != nil {
		logrus.Info("Error")
		render.JSON(w, r, err)
	}
}

func (srv *CourseService) FindHighestVote(w http.ResponseWriter, r *http.Request) {
	courses, err := repo.FindAllCourses()
	HandleError(err)
	course := courses[0]
	for i := 1; i < len(courses); i++ {
		if courses[i].Vote > course.Vote {
			course = courses[i]
		}
	}
	if err := json.NewEncoder(w).Encode(course); err != nil {
		render.JSON(w, r, err)
	}
}

func (srv *CourseService) CreateCourse(w http.ResponseWriter, r *http.Request) {
	var course model.Course
	body, err := getParams(r)
	HandleError(err)
	if err := r.Body.Close(); err != nil {
		render.JSON(w, r, err)
	}
	if err = json.Unmarshal(body, &course); err != nil {
		if err := json.NewEncoder(w).Encode(err); err != nil {
			render.JSON(w, r, err)
		}
	}
	_, errTmp := repo.GetCourseByName(course.Name)
	if errTmp != nil && strings.Contains(errTmp.Error(), "not found") {
		course.Vote = 0
		logrus.Infof("MongoDB: Create new course")
		repo.CreateCourse(&course)
		render.JSON(w, r, course)
	} else {
		logrus.Info("MongoDB: Course already exists")
		render.JSON(w, r, errTmp)
	}
}

func (srv *CourseService) VoteCourse(w http.ResponseWriter, r *http.Request) {
	var course model.Course
	body, err := ioutil.ReadAll(r.Body)
	HandleError(err)
	if err := json.Unmarshal(body, &course); err != nil {
		render.JSON(w, r, err)
	}
	logrus.Info("Course to Vote in Service: ", course)
	logrus.Info("Participant to push in Service; ", course.Participants)
	var courseTmp model.Course
	courseTmp, err = repo.GetCourseByName(course.Name)
	logrus.Info("Get course by name in Service: ", courseTmp)
	if err != nil && strings.Contains(err.Error(), "not found") {
		repo.CreateCourse(&course)
		render.JSON(w, r, course)
		logrus.Info("Create new course: ", course)
		return
	}

	logrus.Info("Length participant currently: ", len(courseTmp.Participants))
	participant := model.Participant{
		Id:    strconv.Itoa(len(courseTmp.Participants)),
		Name:  course.Participants[0].Name,
		Email: course.Participants[0].Email,
	}
	courseTmp.Participants = append(courseTmp.Participants, participant)
	courseTmp.Vote += 1
	logrus.Info("Course temp in Service after plus vote: ", courseTmp)
	logrus.Info("Course's participant temp in Service after plus vote: ", courseTmp.Participants)
	courseTmp, err = repo.Vote(courseTmp)
	if err != nil {
		render.JSON(w, r, err)
		return
	}
	render.JSON(w, r, courseTmp)
}

func (srv *CourseService) DeleteCourseByName(w http.ResponseWriter, r *http.Request) {
	var course model.Course
	body, err := getParams(r)
	HandleError(err)
	if err := r.Body.Close(); err != nil {
		render.JSON(w, r, err)
	}
	if err = json.Unmarshal(body, &course); err != nil {
		if err := json.NewEncoder(w).Encode(err); err != nil {
			render.JSON(w, r, err)
		}
	}
	courseTmp, errTmp := repo.DeleteCourseByName(&course)
	if errTmp != nil {
		render.JSON(w, r, errTmp)
		return
	}
	render.JSON(w, r, courseTmp)
}

func getParams(r *http.Request) ([]byte, error) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func HandleError(err error) {
	if err != nil {
		panic(err)
	}
}
